package selectFromDB;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import sample.CommunicationHelper;

import java.io.IOException;
import java.lang.reflect.Array;
import java.net.URL;
import java.util.*;

import sample.TypeValidator;

import static javax.swing.JOptionPane.showMessageDialog;

public class Controller  implements Initializable {
    private List<String> databaseNameList = new ArrayList<String>();
    private List<String> tableNameList = new ArrayList<String>();
    private List<String> fieldNameList=new ArrayList<String>();
    private List<String> operatorList=new ArrayList<String>();
    private List<String> projectionList=new ArrayList<>();
    private List<String> whereList=new ArrayList<>();
    private List<String> joinedTable=new ArrayList<>();
    private List<String> currentTable=new ArrayList<>();


    private CommunicationHelper ch = new CommunicationHelper();
    private TypeValidator tw = new TypeValidator();


    private ArrayList<String> insertedFieldValue=new ArrayList<String>();

    private ArrayList<String> feltetlek=new ArrayList<>();

    private String currentTableName="Nothing";
    private String addedJoinedTableName="Nothing";
    private ArrayList<String> currentTableElemei =new ArrayList<>();

    //lab6
    private ArrayList<String> groupByProjection =new ArrayList<>(); //listOfGroupbyProjection listanak az elemei
    private ArrayList<String> groupbyCondition =new ArrayList<>(); //listOfGroupbyCondition listanak az elemei
    private ArrayList<String> havingFields =new ArrayList<>(); //listOfHaving listanak az elemei
    private ArrayList<String> havingCondition =new ArrayList<>(); //listOfHavingCondition listanak az elemei

    private String groupByString = "<>";
    private boolean grouped = false;



    @FXML
    private TextField fieldValue;

    @FXML
    private ListView<String> listofDB =new ListView<String>();
    @FXML
    private ListView<String> listofTables=new ListView<String>();
    @FXML
    private ListView<String> listOfFields=new ListView<String>();

    @FXML
    private ListView<String> listofFiledwithValues=new ListView<String>();

    @FXML
    private ListView<String> listOfOperators=new ListView<String>();

    @FXML
    private ListView<String> listOfProjections=new ListView<String>();

    @FXML
    private ListView<String> listOfWhere=new ListView<String>();

    @FXML
    private ListView<String> finalQuery=new ListView<String>();

    @FXML
    private ListView<String> listOfJoinedTable=new ListView<>();

    @FXML
    private ListView<String> listOfCurrentTable=new ListView<>();

    //Lab6 cuccok
    @FXML
    private ListView<String> listOfGroupbyProjection=new ListView<>(); //megadja a group bybol a fieldet amit projectelni akarunk
    @FXML
    private ListView<String>  listOfGroupbyCondition=new ListView<>(); //itt lesznek a conditionok a projectelt fieldekre
    @FXML
    private ListView<String> listOfHaving=new ListView<>(); //ez adja melyik hogy melyik fieldekre lehet feltelt szabni
    @FXML
    private ListView<String> listOfHavingCondition=new ListView<>(); //a havingre a condition
    @FXML
    private TextField conditionValue;



    @Override
    public void initialize(URL url, ResourceBundle rb) //
    {
        listViewCheck();
        loadOperatorList();
        addQueryMessage();
        loadBothConditionList();

    }

    public void addQueryMessage()
    {
        String queryMessage="SELECT ";

        for(int i=0;i<projectionList.size();i++)
        {
            String[] nameSplit=projectionList.get(i).split(" ");
            queryMessage+=nameSplit[0]+" ";
        }

        String table= listofTables.getSelectionModel().getSelectedItem();

        if(table!=null) {
            queryMessage += " FROM " + table+"\n";
        }
        else
        {
            queryMessage+=" FROM \n";
        }

        if(!addedJoinedTableName.equals("Nothing"))
        {

            String joinoltTable = listOfJoinedTable.getSelectionModel().getSelectedItem();
            String currentTable = listOfCurrentTable.getSelectionModel().getSelectedItem(); //pl tabla1.id=tabla2.id -> az id-t nem rakjuk 2x bele

            if (joinoltTable==null)joinoltTable=" ";
            if (currentTable==null)currentTable=" ";

            String[] splitter1=joinoltTable.split(" ");
            String[] splitter2=currentTable.split(" ");

            if (joinoltTable.equals(" ")) splitter1=new String[3]; splitter1[0]=" ";splitter1[1]=" ";splitter1[2]=" ";
            if (currentTable.equals(" ")) splitter2=new String[3]; splitter2[0]=" ";splitter2[1]=" ";splitter2[2]=" ";


            queryMessage+="JOIN "+addedJoinedTableName + " ON " + addedJoinedTableName+"."+splitter1[0]+"="+currentTableName+"."+splitter2[0]+"" +'\n';
        }
        queryMessage+="WHERE ";


        for(int i=0;i<feltetlek.size();i++)
        {
            String[] splitter = feltetlek.get(i).split("#");
            String[] nameSplitter = splitter[0].split(" ");
            if(i==0) {
                queryMessage += (nameSplitter[0] + splitter[1] + splitter[2] + "\n");

            }
            else
            {
                queryMessage += ("AND " + nameSplitter[0] + splitter[1] + splitter[2] + "\n");
            }
        }

        finalQuery.getItems().clear();
        finalQuery.getItems().add(queryMessage);
    }

    public void listViewCheck()
    {
        ch.setMsg("senddbs");
        String dbs = ch.receiveMsg();
        String[] dbslist = dbs.split("#");
        databaseNameList.addAll(Arrays.asList(dbslist));
        listofDB.getItems().addAll(databaseNameList);

        listofFiledwithValues.setMouseTransparent(true);

    }

    @FXML
    public void loadBothConditionList()
    {

        havingCondition.add("MIN");
        havingCondition.add("MAX");
        havingCondition.add("AVG");
        havingCondition.add("COUNT");
        havingCondition.add("SUM");

        groupbyCondition.add("MIN");
        groupbyCondition.add("MAX");
        groupbyCondition.add("AVG");
        groupbyCondition.add("COUNT");
        groupbyCondition.add("SUM");

        listOfGroupbyCondition.getItems().addAll(groupbyCondition);
        listOfHavingCondition.getItems().addAll(havingCondition);
    }

    @FXML
    public void loadOperatorList()
    {
        operatorList.add("=");
        operatorList.add("<");
        operatorList.add("<=");
        operatorList.add(">");
        operatorList.add(">=");

        listOfOperators.getItems().addAll(operatorList);
    }

    @FXML
    public void showTables(MouseEvent arg0) //
    {
        listofTables.getItems().clear();
        tableNameList.clear();

        String databaseClicked= listofDB.getSelectionModel().getSelectedItem();
        ch.setMsg("sendtables "+databaseClicked);
        String tables = ch.receiveMsg();

        String[] tableslist = tables.split("#");
        tableNameList.addAll(Arrays.asList(tableslist));

        listofTables.getItems().addAll(tableNameList);
    }


    public void showFields(MouseEvent arg0)
    {

        listOfFields.getItems().clear();
        fieldNameList.clear();
        insertedFieldValue.clear();

        String dbNameString = listofDB.getSelectionModel().getSelectedItem();
        String tableNameString = listofTables.getSelectionModel().getSelectedItem();

        if ((dbNameString == null) || (tableNameString == null)) {
            showMessageDialog(null, "One of the elements (database,table) has not been selected. Try again!");
        } else {
            //addQueryMessage();

            String databaseClicked= listofDB.getSelectionModel().getSelectedItem();
            ch.setMsg("sendfields "+tableNameString+"#"+dbNameString);
            String fields = ch.receiveMsg();
            String[] fieldslist = fields.split("#");
            fieldNameList.addAll(Arrays.asList(fieldslist));
            fieldNameList.add("_id ObjectID PK");
            listOfFields.getItems().addAll(fieldNameList);


            for(int i=0;i<fieldNameList.size();i++)
            {
                insertedFieldValue.add("null");
            }

            //refreshFieldValues();

        }

    }

    @FXML
    public void addFieldValue() {

        String fieldValueString = fieldValue.getText();
        if (fieldValueString.isEmpty()) {
            showMessageDialog(null, "The field is empty. Write a value in it!");
        } else {


            String whereField = listOfWhere.getSelectionModel().getSelectedItem();
            String operatorField=listOfOperators.getSelectionModel().getSelectedItem();

            if ((whereField == null) || (operatorField == null)) {
                showMessageDialog(null, "One of the elements (where field,operator) has not been selected. Try again!");
            }
            else
            {
                String[] parser=whereField.split(" ");
                String fieldType=parser[1];
                boolean ok=true;

                if(fieldType.equals("Int") && tw.isInt(fieldValueString) == false)
                {
                    showMessageDialog(null, "Invalid Type. The requested is Integer");
                    ok=false;

                }

                if(fieldType.equals("Float") && tw.isFloat(fieldValueString) == false)
                {
                    showMessageDialog(null, "Invalid Type. The requested is Float");
                    ok=false;


                }

                if(fieldType.equals("String") && tw.isString(fieldValueString) == false)
                {
                    showMessageDialog(null, "Invalid Type. The requested is String");
                    ok=false;
                }
                if(fieldType.equals("Date") && tw.isDate(fieldValueString) == false)
                {
                    showMessageDialog(null, "Invalid Type. The requested is Date");
                    ok=false;
                }

                if(ok) {
                    feltetlek.add(whereField + "#" + operatorField + "#" + fieldValueString);
                    addQueryMessage();
                }
            }
        }
    }

    @FXML
    public void removeFieldValue()
    {
        String fieldValueString = fieldValue.getText();
        if (fieldValueString.isEmpty()) {
            showMessageDialog(null, "The field is empty. Write a value in it!");
        } else {

            String whereField = listOfWhere.getSelectionModel().getSelectedItem();
            String operatorField=listOfOperators.getSelectionModel().getSelectedItem();

            if ((whereField == null) || (operatorField == null)) {
                showMessageDialog(null, "One of the elements (where field,operator) has not been selected. Try again!");
            }
            else
            {
                String[] parser=whereField.split(" ");
                String fieldType=parser[1];
                boolean ok=true;

                if(fieldType.equals("Int") && tw.isInt(fieldValueString) == false)
                {
                    showMessageDialog(null, "Invalid Type. The requested is Integer");
                    ok=false;

                }

                if(fieldType.equals("Float") && tw.isFloat(fieldValueString) == false)
                {
                    showMessageDialog(null, "Invalid Type. The requested is Float");
                    ok=false;


                }

                if(fieldType.equals("String") && tw.isString(fieldValueString) == false)
                {
                    showMessageDialog(null, "Invalid Type. The requested is String");
                    ok=false;
                }
                if(fieldType.equals("Date") && tw.isDate(fieldValueString) == false)
                {
                    showMessageDialog(null, "Invalid Type. The requested is Date");
                    ok=false;
                }

                if(ok) {
                    boolean talaltIlyet=false;
                    for(int i=0;i<feltetlek.size();i++) {
                        String[] szovegSplit=feltetlek.get(i).split("#");
                        if(feltetlek.get(i).equals(whereField+"#"+operatorField+"#"+fieldValueString))
                        {
                            feltetlek.remove(i);
                            talaltIlyet=true;
                            addQueryMessage();
                        }
                    }
                    if(talaltIlyet==false)
                    {
                        showMessageDialog(null, "You wanted to delete a where condition which does not exists. Try again");
                    }
                }
            }
        }
    }


    @FXML
    public void addToProjection()
    {
        listOfProjections.getItems().clear();

        String fieldNameString = listOfFields.getSelectionModel().getSelectedItem();

            projectionList.add(fieldNameString);
            listOfProjections.getItems().addAll(projectionList);

            //UPDATEOLNI A SELECTET IS
            addQueryMessage();

            //LAB6 - Amilyen field projectiont adunk a where-hez az megy a groupbyhoz is

        if(!currentTableName.equals("JoinedOnce")) { //csak akkor adom a groupby fieldek koze a projectionolteket ha mar nem volt joinolva egyszer
            //ha volt akkor a current tablet kapja meg
            listOfGroupbyProjection.getItems().clear();
            groupByProjection.add(fieldNameString);
            listOfGroupbyProjection.getItems().addAll(groupByProjection);
        }

    }


    @FXML
    public void removeFromProjection()
    {
        String fieldNameString = listOfProjections.getSelectionModel().getSelectedItem();
        listOfProjections.getItems().clear();
        projectionList.remove(fieldNameString);
        listOfProjections.getItems().addAll(projectionList);


        addQueryMessage();

        //LAB6 - Amilyen field projectiont removeolunk a where-bol az removeoloduk a groupbybol is
        listOfGroupbyProjection.getItems().clear();
        groupByProjection.remove(fieldNameString);
        listOfGroupbyProjection.getItems().addAll(groupByProjection);
    }


    @FXML
    public void addToWhere()
    {

        listOfWhere.getItems().clear();

        String fieldNameString = listOfFields.getSelectionModel().getSelectedItem();
        if (fieldNameString.equals("_id ObjectID PK")){
            showMessageDialog(null, "_id field can only be projected!");
        }else {
            whereList.add(fieldNameString);
            listOfWhere.getItems().addAll(whereList);
        }
    }

    @FXML
    public void removeFromWhere()
    {
        String fieldNameString = listOfWhere.getSelectionModel().getSelectedItem();
        listOfWhere.getItems().clear();
        whereList.remove(fieldNameString);
        listOfWhere.getItems().addAll(whereList);

        for(int i=0;i<feltetlek.size();i++)
        {
            String[] szovegSplit=feltetlek.get(i).split("#");

            if(szovegSplit[0].equals(fieldNameString))
            {
                feltetlek.remove(i);
            }
        }
        addQueryMessage();
    }

    @FXML
    public void doSelect()
    {
        System.out.println("Itt tortenik az egesz query lementese, mikor a select buttonre klikkel");
        String dbNeve=listofDB.getSelectionModel().getSelectedItem();
        String tablaNeve=listofTables.getSelectionModel().getSelectedItem();

        if ((dbNeve == null) || (tablaNeve == null)) {
            showMessageDialog(null, "One of the elements (where field,operator) has not been selected. Try again!");
        }
        else {

            String projectfields = "";
            System.out.println("project");
            for (int i = 0; i < projectionList.size(); i++) {
                System.out.println(projectionList.get(i));
                projectfields=projectfields+projectionList.get(i)+"#";
            }

            String wherefields = "";
            System.out.println("where");
            for (int i = 0; i < feltetlek.size(); i++) {
                System.out.println(feltetlek.get(i));
                wherefields=wherefields+feltetlek.get(i)+"@";
            }



            String response = dbNeve+"%"+tablaNeve+"%"+projectfields+"%"+wherefields+groupByString;
            if (grouped){
                grouped=false;
                groupByString="<>";
            }
            System.out.println("final response");
            System.out.println(response);


            ch.setMsg("select "+response);
            ch.sendMsg();

            showMessageDialog(null, "Query results saved to queries.txt (C:\\ABKR)");
        }
    }

    @FXML
    public void addJoinedTable(){

        System.out.println("Itt adom hozza a table fieldjeit a joinedtableshez");
        listOfJoinedTable.getItems().clear();
        joinedTable.clear();

        String dbNameString = listofDB.getSelectionModel().getSelectedItem();
        String tableNameString = listofTables.getSelectionModel().getSelectedItem();

        addedJoinedTableName=tableNameString; //ez kell a joinolashoz hogy tudjuk mi a hozzadott tabla neve (collection miatt)

        ch.setMsg("sendfields "+tableNameString+"#"+dbNameString);
        String fields = ch.receiveMsg();
        System.out.println(fields);
        String[] fieldslist = fields.split("#");
        joinedTable.addAll(Arrays.asList(fieldslist));
        joinedTable.add("_id ObjectID PK");

        listOfJoinedTable.getItems().addAll(joinedTable);
    }

    @FXML
    public void joinTables(){
        addQueryMessage();
        // System.out.println("Itt joinolom ossze a ket tablat");
        String dbNameString = listofDB.getSelectionModel().getSelectedItem();
        String[] joinedTableElemei;
        String joinoltFieldek = listOfJoinedTable.getSelectionModel().getSelectedItem(); //pl tabla1.id=tabla2.id -> az id-t nem rakjuk 2x bele


        //STEP1: Lekerni a field valuekat
        if(!currentTableName.equals("JoinedOnce")) {
            String tableNameString = "TEMPTABLE"; //ha ez az elso join letrehoz egy temporalis tablat az uj join erteknek
            String send = tableNameString + "#" + dbNameString;
            ch.setMsg("create table " + send);
            ch.sendMsg();

            if (joinoltFieldek.equals("_id ObjectID PK")) {
            ch.setMsg("GETKEYS " + dbNameString + " " + addedJoinedTableName);
            } else {
            ch.setMsg("GETFIELDVALUES " + dbNameString + " " + addedJoinedTableName);
            }
            String values=ch.receiveMsg();
            joinedTableElemei=values.split("@");
            System.out.println("ez a joined table elemei: " + Arrays.toString(joinedTableElemei));

            ch.setMsg("GETFIELDVALUES "+dbNameString+" "+currentTableName);
            String valuesOfCurTable=ch.receiveMsg();
            String[] splittedValues=valuesOfCurTable.split("@");
            //currentTableElemei=valuesOfCurTable.split("@");

            for(int i=0;i<splittedValues.length;i++)
            {
                currentTableElemei.add(splittedValues[i]);
            }
            System.out.println("ez a current table elemei: " + currentTableElemei.toString());
            //leelenorzom, hogy mar joinolva volt-e a jelenlegi tabla ha nem le kell kerjem a collection adatait, ha igen a jelenlegi allassal dolgozok tovabb
            currentTableName="JoinedOnce";
        }
        else //ha mar volt egyszer joinolva csak a jelenlegi tabla elemeit kerjuk le, a currenttable a mar joinolt tablak osszetetele
        {

            //ha mar volt join egyszer akkor kitorlom a temptablat es letrehozok egy ujat
            ch.setMsg("drop table "+"TEMPTABLE"+"#"+dbNameString);
            ch.sendMsg();

            String tableNameString = "TEMPTABLE";
            String send = tableNameString+"#"+dbNameString;
            ch.setMsg("create table "+send);
            ch.sendMsg();

            //////////////////////////////

            if (joinoltFieldek.equals("_id ObjectID PK")) {
                ch.setMsg("GETKEYS " + dbNameString + " " + addedJoinedTableName);
            } else {
                ch.setMsg("GETFIELDVALUES " + dbNameString + " " + addedJoinedTableName);
            }
            String values=ch.receiveMsg();
            joinedTableElemei=values.split("@");
            System.out.println("ez a joined table elemei: " + Arrays.toString(joinedTableElemei));
        }

        //STEP2:Osszefuzzuk a joinolni kivant tabla oszlopait + a jelenlegi tabla fieldjeit egy ujba
        ArrayList<String> newTable=new ArrayList<>();


//        if(joinoltFieldek.equals("_id ObjectID PK")){
//
//        }


        for(int i=0;i<currentTable.size();i++)
        {
            newTable.add(currentTable.get(i));
        }

        for(int i=0;i<joinedTable.size();i++)
        {
            if(!joinedTable.get(i).equals(joinoltFieldek))
            {
                newTable.add(joinedTable.get(i));
            }
        } //jelenlegi joinolas -> currentabla fieldjei, joinolt tabla fieldjei (-az id amire joinolunk)



        //STEP3:JOINOLJUK A VALUEKAT IS

        //a) lekerjuk mindket tablabol a ket join feltel poziciojat
        String currentFieldSelected = listOfCurrentTable.getSelectionModel().getSelectedItem(); //pl tabla1.id=tabla2.id -> az id-t nem rakjuk 2x bele

        int currentTableIndex=currentTable.indexOf(currentFieldSelected);
        int joinedTableIndex=joinedTable.indexOf(joinoltFieldek);


        //System.out.println("TableIndex= "+joinedTableIndex+ "CurrentIndex= "+currentTableIndex);

        ArrayList <String> newTableValues = new ArrayList<>();

        for(int i=0;i<currentTableElemei.size();i++)
        {
            for(int j=0;j<joinedTableElemei.length;j++)
            {
                //megkapjuk mindket dokumentbol 1-1 sort, felsplitteljuk hogy az index elemeket ossze tudjuk hasonlitani
                String[] currentTablaFieldjei=currentTableElemei.get(i).split("%");
                String[] joinoltTablaFieldjei;
                if (joinoltFieldek.equals("_id ObjectID PK")) { //ha pk-rol van szo akkor csak siman van egy pk listank.
                    joinoltTablaFieldjei = joinedTableElemei;
                    joinedTableIndex=j;
                }else {
                    joinoltTablaFieldjei = joinedTableElemei[j].split("%");
                }
                //System.out.println("joinolt table fieldjei = "+ Arrays.toString(joinoltTablaFieldjei));




                if(joinoltTablaFieldjei[joinedTableIndex].equals(currentTablaFieldjei[currentTableIndex]))
                {
                    String finalValue="";
                    //ha a ket ertek egyenlo -> joinoljuk a valuekat
                    for(int k=0;k<currentTablaFieldjei.length;k++)
                    {
                        finalValue+=(currentTablaFieldjei[k]+"%");
                    }

                    for(int k=0;k<joinoltTablaFieldjei.length;k++)
                    {
                        if(k!=joinedTableIndex)
                        {
                            finalValue+=(joinoltTablaFieldjei[k]+"%");
                        }
                    }

                    newTableValues.add(finalValue);
                }
            }
        }


        currentTable.clear();

        String fieldNames="";
        String fieldTypes="";
        String fieldValues="";

        for(int i=0;i<newTable.size();i++)
        {
            String[] splitStr=newTable.get(i).split(" ");
            fieldNames+=(splitStr[0]+"%");
            if (splitStr[2].equals("FK")) splitStr[2]=("None");
            fieldTypes+=(splitStr[2]+"@");
            currentTable.add(newTable.get(i));

            //LAB6- Ami a joinolas utan megy a current tablaba az jon ide is, a fieldek ertekei maradhatnak a currenttablebe majd a groupnal azokkal dolgozunk
            //havingFields.add(newTable.get(i));

            if(splitStr.length==4)
            {
                //nev,tipus,tabla,db,keyrestriciton,fk
                ch.setMsg("addfield "+splitStr[0]+ "#" +splitStr[1]+ "#" + "TEMPTABLE" + "#" + dbNameString+ "#" +splitStr[2]+ "#" +splitStr[3]);
            }
            else
            {
                ch.setMsg("addfield "+splitStr[0]+"#"+splitStr[1]+"#"+"TEMPTABLE"+"#"+dbNameString+"#"+splitStr[2]+"#"+null);
            }
            ch.sendMsg();
        }

        System.out.println();
        currentTableElemei.clear();


        for (int j=0;j<newTableValues.size();j++)
        {
            currentTableElemei.add(newTableValues.get(j));
            fieldValues=newTableValues.get(j);
            //  System.out.println(fieldValues);
            //  System.out.println(fieldNames);
            //  System.out.println(fieldTypes);
            ch.setMsg("insert "+"TEMPTABLE"+"#"+dbNameString+"#"+fieldValues+"#"+fieldNames+"#"+fieldTypes);
            ch.sendMsg();
        }
        refreshCurrentValues();
    }

    public void refreshCurrentValues() {
        listOfCurrentTable.getItems().clear();
        currentTable.remove("_id ObjectID PK");
        listOfCurrentTable.getItems().addAll(currentTable);
        //listOfCurrentTable.getItems().remove("_id ObjectID PK");

        //LAB6 - A havingunket is refresheljuk
        listOfHaving.getItems().clear();
        havingFields.addAll(currentTable);
        listOfHaving.getItems().addAll(havingFields);

        listOfGroupbyProjection.getItems().clear();
        groupByProjection.clear();
        groupByProjection.addAll(currentTable);
        listOfGroupbyProjection.getItems().addAll(groupByProjection);
    }

    @FXML
    public void removeFromJoinedTable()
    {
        //System.out.println("Removeolom a fieldjeit a joinedtablebol");
        listOfJoinedTable.getItems().clear();
        joinedTable.clear();
    }

    @FXML void addToCurrentTable()
    {
        listOfCurrentTable.getItems().clear();
        currentTable.clear();

        String dbNameString = listofDB.getSelectionModel().getSelectedItem();
        String tableNameString = listofTables.getSelectionModel().getSelectedItem();

        currentTableName=tableNameString; //kell a joinhoz

        ch.setMsg("sendfields "+tableNameString+"#"+dbNameString);
        String fields = ch.receiveMsg();
        String[] fieldslist = fields.split("#");
        currentTable.addAll(Arrays.asList(fieldslist));

        listOfCurrentTable.getItems().addAll(currentTable);
    }

    @FXML
    public void backToMain(ActionEvent event) throws IOException
    {
        String dbNameString=listofDB.getSelectionModel().getSelectedItem();
        ch.setMsg("droptempt TEMPTABLE#"+dbNameString);
        ch.sendMsg();

        Parent tableViewParent = FXMLLoader.load(getClass().getResource("../sample/sample.fxml"));
        Scene tableViewScene = new Scene(tableViewParent);

        //This line gets the Stage information
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();

        window.setScene(tableViewScene);
        window.show();
    }

    @FXML
    public void groupTables() {
        System.out.println("Itt lesz a final groupby");
        String projectionField = listOfGroupbyProjection.getSelectionModel().getSelectedItem(); //a field amit projectelni akar
        String projectionCondition = listOfGroupbyCondition.getSelectionModel().getSelectedItem(); //annak a conditionje
        String havingProjField = listOfHaving.getSelectionModel().getSelectedItem(); //having field
        String havingConditionField = listOfHavingCondition.getSelectionModel().getSelectedItem(); //having condition
        String condValue = conditionValue.getText();


        if ((havingProjField != null) && condValue.isEmpty()) {
            showMessageDialog(null, "The field is empty. Write a value in it!");
        }

        if( (projectionField !=null) && (projectionCondition==null))
        {
            showMessageDialog(null, "You have to select the group by condition for it");
        }

        if( (havingProjField !=null) && (havingConditionField==null))
        {
            showMessageDialog(null, "You have to select the having condition for it");
        }

        if (havingProjField != null) {
            String[] parser = havingProjField.split(" ");
            String fieldType = parser[1];
            boolean ok = true;

            if (fieldType.equals("Int") && tw.isInt(condValue) == false) {
                showMessageDialog(null, "Invalid Type. The requested is Integer");
                ok = false;

            }

            if (fieldType.equals("Float") && tw.isFloat(condValue) == false) {
                showMessageDialog(null, "Invalid Type. The requested is Float");
                ok = false;
            }

            if (fieldType.equals("String") && tw.isString(condValue) == false) {
                showMessageDialog(null, "Invalid Type. The requested is String");
                ok = false;
            }
            if (fieldType.equals("Date") && tw.isDate(condValue) == false) {
                showMessageDialog(null, "Invalid Type. The requested is Date");
                ok = false;
            }

            if (ok) {
                System.out.println("Erre van groupby: "+projectionField);
                System.out.println("Ilyen muvelet kell a groupby-ra "+ projectionCondition);
                System.out.println("Erre van having: "+havingProjField);
                System.out.println("Ilyen muvelet kell a havingre "+havingConditionField);
                System.out.println("Ez a value aminek meg kell feleljen "+condValue);

                System.out.println(projectionField+"#"+projectionCondition+"#"+havingProjField+"#"+havingConditionField+"#"+condValue);
                grouped = true;
                groupByString="<>"+projectionField+"#"+projectionCondition+"#"+havingProjField+"#"+havingConditionField+"#"+condValue;
                //feltetlek.add(whereField + "#" + operatorField + "#" + fieldValueString);
                //addQueryMessage();
            }
        }
    }

}